<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Make Appointment                   _57f49e</name>
   <tag></tag>
   <elementGuidId>bdc93395-48c5-493e-810c-6c17dc97635a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#appointment</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>42700783-7400-4326-8d00-51de96110bcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>appointment</value>
      <webElementGuid>74539f51-6b8c-4bf9-b8ed-cc9d579e3c16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>section bg-primary</value>
      <webElementGuid>804368ee-d440-496c-8ab6-76c7bd661027</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
</value>
      <webElementGuid>05414481-8a7c-40d9-a32d-01f58fbb8895</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)</value>
      <webElementGuid>75313df8-7334-412e-9dfa-f1d7726f81c8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//section[@id='appointment']</value>
      <webElementGuid>37b10d95-bb78-4107-b9e1-cce09f1894fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::section[1]</value>
      <webElementGuid>91543945-8bf1-494e-8cf6-4143b3b81086</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::section[1]</value>
      <webElementGuid>7995b3ce-65ef-456f-828c-6be74ad01ebe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>afa2467c-b389-4670-bb19-2cafe1594823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[@id = 'appointment' and (text() = '
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
' or . = '
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
')]</value>
      <webElementGuid>5c576cd2-ba58-461a-a876-20ddd7ce5bf1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
